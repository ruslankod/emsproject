package ems.users;

/**
 * Класс хранит данные о студентах (имя, фамилия, адрес электронной почты, пароль, изучаемый предмет),
 * и сортирует данные сначала по полю city, а затем по полю name.
 */
public class Students implements Comparable<Students>{
    String name;
    String surname;
    String email;
    String password;
    String subject;


    /**
     * Конструктор класса, создающий экземпляр с набором данных по студентам.
     * @param name имя, отличное от null.
     * @param surname фамилия, отличная от null.
     * @param email адрес электронной почты.
     * @param password пароль.
     * @param subject изучаемый предмет.
     */
    public Students(String name, String surname, String email, String password, String subject) {
        if (name == null || surname == null || email == null || password == null) {
            throw new IllegalArgumentException("Name, Surname, E-mail and Password are required");
        } else {
            this.name = name;
            this.surname = surname;
            this.email = email;
            this.password = password;
            this.subject = subject;
        }
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getSubject() {
        return subject;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name ='" + name + '\'' +
                ", surname =" + surname +
                ", subject ='" + subject + '\'' +
                '}';
    }

    /**
     * Сортировка сначала по полю subject, а затем по полю surname.
     * @param other принимает массив или коллекцию элементов одного класса.
     * @return упорядоченные по двум полям списки элементов
     */
    @Override
    public int compareTo(Students other) {
        int cmp = this.subject.compareToIgnoreCase(other.subject);
        if (cmp == 0) {
            return this.surname.compareToIgnoreCase(other.surname);
        } else {
            return cmp;
        }
    }
}