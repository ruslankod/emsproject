package ems.users;

public class Admins {
    String name;
    String surname;
    String email;
    String password;


    /**
     * Конструктор класса, создающий экземпляр с набором данных по студентам.
     * @param name имя, отличное от null.
     * @param surname фамилия, отличная от null.
     * @param email адрес электронной почты.
     * @param password пароль.
     */
    public Admins(String name, String surname, String email, String password) {
        if (name == null || surname == null || email == null || password == null) {
            throw new IllegalArgumentException("Name, Surname, E-mail and Password are required");
        } else {
            this.name = name;
            this.surname = surname;
            this.email = email;
            this.password = password;
        }
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "Admin{" +
                "name ='" + name + '\'' +
                ", surname =" + surname +
                ", email ='" + email + '\'' +
                '}';
    }
}