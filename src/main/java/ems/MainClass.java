package ems;

import com.sun.net.httpserver.HttpServer;
import ems.dao.AdminsDAO;
import ems.dao.StudentsDAO;
import ems.dao.TeachersDAO;
import ems.handlers.AdminsHandler;
import ems.handlers.StudentHandler;
import ems.handlers.TeachersHandler;
import ems.users.Admins;
import ems.users.Students;
import ems.users.Teachers;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.sql.SQLException;
import java.util.concurrent.Executors;

public class MainClass {

    private static final String HOSTNAME = "localhost";
    private static final int PORT = 8000;

    public static void main(String[] args) throws SQLException, IOException
    {
        Students student1 = new Students("Pavel", "Ablogin", "mail1@mail.ru", "12345", "Java");
        Students student2 = new Students("Dmitry", "Balikhin", "mail2@mail.ru", "12345", "Java");
        Teachers teacher1 = new Teachers("Roman", "Taranov", "teacher1@mail.ru", "54321", "Java");
        Teachers teacher2 = new Teachers("Anton", "Trenkunov", "teacher2@mail.ru", "54321", "Java");
        Admins admin = new Admins("Admin", "Adminov", "admin@mail.ru", "admin123");
        StudentsDAO studentsDAO = new StudentsDAO();
        TeachersDAO teachersDAO = new TeachersDAO();
        AdminsDAO adminsDAO = new AdminsDAO();
        studentsDAO.adStudent(student1);
        studentsDAO.adStudent(student2);
        teachersDAO.adTeacher(teacher1);
        teachersDAO.adTeacher(teacher2);
        adminsDAO.adAdmin(admin);

//        HttpServer server = HttpServer.create(new InetSocketAddress(HOSTNAME, PORT), 0);
//
//        server.createContext("/students", new StudentHandler(new StudentsDAO()));
//        server.createContext("/teachers", new TeachersHandler(new TeachersDAO()));
//        server.createContext("/admins", new AdminsHandler(new AdminsDAO()));
//        server.setExecutor(Executors.newSingleThreadExecutor());
//
//        server.start();
//        System.out.println("Simple server started...");
    }
}

