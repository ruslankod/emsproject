package ems.dao;

import ems.users.Admins;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class AdminsDAO {
    public static final String jdbcURL = "jdbc:sqlite:F:\\Pro\\SBER\\EMS\\emsproject\\emsDB.db";

    /**gi
     * Метод добавляет данные объекта в таблицу.
     * @param admin - объект класса Admins.
     * @return true - если данные успешно добавлены.
     */
    public boolean adAdmin(Admins admin) {
        final String createAdminQuery = "INSERT INTO admins(name, surname, email, password) VALUES (?,?,?,?)";

        try (Connection connection = DriverManager.getConnection(jdbcURL);
             PreparedStatement statement = connection.prepareStatement(createAdminQuery)) {

            statement.setString(1, admin.getName());
            statement.setString(2, admin.getSurname());
            statement.setString(3, admin.getEmail());
            statement.setString(4, admin.getPassword());
            statement.executeUpdate();

        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * Метод считывает все данные, содержащиеся в таблице.
     * @return возвращает все данные таблицы в виде списка,
     * отсортированные по столбцу id в порядке возрастания.
     */
    public Collection<Admins> readAllAdmins() {
        final String readAllAdminsQuery = "SELECT * FROM admins ORDER BY id ASC";

        try (Connection connection = DriverManager.getConnection(jdbcURL);
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(readAllAdminsQuery)) {

            List<Admins> adminList = new ArrayList<>();

            while (resultSet.next()) {
                String name = resultSet.getString("name");
                String surname = resultSet.getString("surname");
                String email = resultSet.getString("email");
                String password = resultSet.getString("password");
                String subject = resultSet.getString("subject");
                Admins localAdmin = new Admins(name, surname, email, password);
                adminList.add(localAdmin);
            }
            return adminList;
        } catch (SQLException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    /**
     * Метод обновляет данные в таблице по указанному номеру id.
     * @param admin - объект класса Admins, содержащий новые данные.
     * @param id - номер обновляемой строки таблицы.
     */
    public void updateAdmin(Admins admin, int id) {
        final String updateQuery = "UPDATE admins "
                + "SET "
                + "name = ?, "
                + "surname = ?, "
                + "email = ?, "
                + "password = ? "
                + "WHERE "
                + "id = "
                + id;

        try (Connection connection = DriverManager.getConnection(jdbcURL);
             PreparedStatement statement = connection.prepareStatement(updateQuery)) {

            statement.setString(1, admin.getName());
            statement.setString(2, admin.getSurname());
            statement.setString(3, admin.getEmail());
            statement.setString(4, admin.getPassword());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод удаляет строку в таблице с заданным номером id.
     * @param id - номер удаляемой строки таблицы.
     */
    public void deleteAdmin(int id) {
        final String deleteQuery = "DELETE FROM admins WHERE id = " + id;
        try (Connection connection = DriverManager.getConnection(jdbcURL);
             Statement statement = connection.createStatement()) {

            statement.execute(deleteQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
