package ems.dao;

import ems.users.Students;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Класс содержит методы для записи, чтения, обновления и удаления
 * данных в таблицу со студентами, хранящуюся в базе данных
 */
public class StudentsDAO {
    public static final String jdbcURL = "jdbc:sqlite:F:\\Pro\\SBER\\EMS\\emsproject\\emsDB.db";

    /**gi
     * Метод добавляет данные объекта в таблицу.
     * @param student - объект класса Students.
     * @return true - если данные успешно добавлены.
     */
    public boolean adStudent(Students student) {
        final String createStudentQuery = "INSERT INTO students(name, surname, email, password, subject) VALUES (?,?,?,?,?)";

        try (Connection connection = DriverManager.getConnection(jdbcURL);
             PreparedStatement statement = connection.prepareStatement(createStudentQuery)) {

            statement.setString(1, student.getName());
            statement.setString(2, student.getSurname());
            statement.setString(3, student.getEmail());
            statement.setString(4, student.getPassword());
            statement.setString(5, student.getSubject());
            statement.executeUpdate();

        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * Метод считывает все данные, содержащиеся в таблице.
     * @return возвращает все данные таблицы в виде списка,
     * отсортированные по столбцу id в порядке возрастания.
     */
    public Collection<Students> readAllStudents() {
        final String readAllStudentsQuery = "SELECT * FROM students ORDER BY id ASC";

        try (Connection connection = DriverManager.getConnection(jdbcURL);
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(readAllStudentsQuery)) {

            List <Students> studentList = new ArrayList<>();

            while (resultSet.next()) {
                String name = resultSet.getString("name");
                String surname = resultSet.getString("surname");
                String email = resultSet.getString("email");
                String password = resultSet.getString("password");
                String subject = resultSet.getString("subject");
                Students localStudent = new Students(name, surname, email, password, subject);
                studentList.add(localStudent);
            }
            return studentList;
        } catch (SQLException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    /**
     * Метод обновляет данные в таблице по указанному номеру id.
     * @param student - объект класса Students, содержащий новые данные.
     * @param id - номер обновляемой строки таблицы.
     */
    public void updateStudent(Students student, int id) {
        final String updateQuery = "UPDATE students "
                + "SET "
                + "name = ?, "
                + "surname = ?, "
                + "email = ?, "
                + "password = ? "
                + "subject = ? "
                + "WHERE "
                + "id = "
                + id;

        try (Connection connection = DriverManager.getConnection(jdbcURL);
             PreparedStatement statement = connection.prepareStatement(updateQuery)) {

            statement.setString(1, student.getName());
            statement.setString(2, student.getSurname());
            statement.setString(3, student.getEmail());
            statement.setString(4, student.getPassword());
            statement.setString(5, student.getSubject());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод удаляет строку в таблице с заданным номером id.
     * @param id - номер удаляемой строки таблицы.
     */
    public void deleteStudent(int id) {
        final String deleteQuery = "DELETE FROM students WHERE id = " + id;
        try (Connection connection = DriverManager.getConnection(jdbcURL);
             Statement statement = connection.createStatement()) {

            statement.execute(deleteQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}