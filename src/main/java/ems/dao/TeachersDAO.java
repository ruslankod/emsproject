package ems.dao;

import ems.users.Teachers;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class TeachersDAO {
    public static final String jdbcURL = "jdbc:sqlite:F:\\Pro\\SBER\\EMS\\emsproject\\emsDB.db";

    /**
     * Метод добавляет данные объекта в таблицу.
     * @param teacher - объект класса Teachers.
     * @return true - если данные успешно добавлены.
     */
    public boolean adTeacher(Teachers teacher) {
        final String createTeacherQuery = "INSERT INTO teachers(name, surname, email, password, subject) VALUES (?,?,?,?,?)";

        try (Connection connection = DriverManager.getConnection(jdbcURL);
             PreparedStatement statement = connection.prepareStatement(createTeacherQuery)) {

            statement.setString(1, teacher.getName());
            statement.setString(2, teacher.getSurname());
            statement.setString(3, teacher.getEmail());
            statement.setString(4, teacher.getPassword());
            statement.setString(5, teacher.getSubject());
            statement.executeUpdate();

        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * Метод считывает все данные, содержащиеся в таблице.
     * @return возвращает все данные таблицы в виде списка,
     * отсортированные по столбцу id в порядке возрастания.
     */
    public Collection<Teachers> readAllTeachers() {
        final String readAllTeachersQuery = "SELECT * FROM teachers ORDER BY id ASC";

        try (Connection connection = DriverManager.getConnection(jdbcURL);
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(readAllTeachersQuery)) {

            List<Teachers> teacherList = new ArrayList<>();

            while (resultSet.next()) {
                String name = resultSet.getString("name");
                String surname = resultSet.getString("surname");
                String email = resultSet.getString("email");
                String password = resultSet.getString("password");
                String subject = resultSet.getString("subject");
                Teachers localTeachers = new Teachers(name, surname, email, password, subject);
                teacherList.add(localTeachers);
            }
            return teacherList;
        } catch (SQLException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    /**
     * Метод обновляет данные в таблице по указанному номеру id.
     * @param teacher - объект класса Teachers, содержащий новые данные.
     * @param id - номер обновляемой строки таблицы.
     */
    public void updateTeacher(Teachers teacher, int id) {
        final String updateQuery = "UPDATE teachers "
                + "SET "
                + "name = ?, "
                + "surname = ?, "
                + "email = ?, "
                + "password = ? "
                + "subject = ? "
                + "WHERE "
                + "id = "
                + id;

        try (Connection connection = DriverManager.getConnection(jdbcURL);
             PreparedStatement statement = connection.prepareStatement(updateQuery)) {

            statement.setString(1, teacher.getName());
            statement.setString(2, teacher.getSurname());
            statement.setString(3, teacher.getEmail());
            statement.setString(4, teacher.getPassword());
            statement.setString(5, teacher.getSubject());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод удаляет строку в таблице с заданным номером id.
     * @param id - номер удаляемой строки таблицы.
     */
    public void deleteTeacher(int id) {
        final String deleteQuery = "DELETE FROM teachers WHERE id = " + id;
        try (Connection connection = DriverManager.getConnection(jdbcURL);
             Statement statement = connection.createStatement()) {

            statement.execute(deleteQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}