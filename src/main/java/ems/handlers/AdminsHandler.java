package ems.handlers;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import ems.dao.AdminsDAO;
import ems.users.Admins;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

public class AdminsHandler implements HttpHandler {
    private final AdminsDAO dao;

    public AdminsHandler(AdminsDAO dao)
    {
        this.dao = dao;
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException
    {
        OutputStream outputStream = exchange.getResponseBody();

        StringBuilder htmlBuilder = new StringBuilder();

        if (exchange.getRequestMethod().equalsIgnoreCase("GET"))
        {
            htmlBuilder.append("<html>")
                    .append("<body>")
                    .append("<h1>")
                    .append("All admins from Data Base: " + getMethod())
                    .append("</h1>")
                    .append("</body>")
                    .append("</html>");
        }
        else if (exchange.getRequestMethod().equalsIgnoreCase("POST"))
        {
            String jsonAdmin =
                    new BufferedReader(new InputStreamReader(exchange.getRequestBody()))
                            .lines()
                            .collect(Collectors.joining());

            JSONObject jsonObject = new JSONObject(jsonAdmin);

            String name = jsonObject.toMap().get("name").toString();
            String surname = jsonObject.toMap().get("surname").toString();
            String email = jsonObject.toMap().get("email").toString();
            String password = jsonObject.toMap().get("password").toString();
            Admins admin = new Admins(name, surname, email, password);

            htmlBuilder.append("<html>")
                    .append("<body>")
                    .append("<h1>")
                    .append("Create new admin in DB: " + postMethod(admin))
                    .append("</h1>")
                    .append("</body>")
                    .append("</html>");
        }
        else if (exchange.getRequestMethod().equalsIgnoreCase("PUT"))
        {
            String jsonAdmin =
                    new BufferedReader(new InputStreamReader(exchange.getRequestBody()))
                            .lines()
                            .collect(Collectors.joining());

            JSONObject jsonObject = new JSONObject(jsonAdmin);

            String name = jsonObject.toMap().get("name").toString();
            String surname = jsonObject.toMap().get("surname").toString();
            String email = jsonObject.toMap().get("email").toString();
            String password = jsonObject.toMap().get("password").toString();
            Admins admin = new Admins(name, surname, email, password);
            int id = jsonObject.getInt("id");

            htmlBuilder.append("<html>")
                    .append("<body>")
                    .append("<h1>")
                    .append("Update admin by ID in DB: " + updateMethod(admin, id))
                    .append("</h1>")
                    .append("</body>")
                    .append("</html>");
        }
        else if (exchange.getRequestMethod().equalsIgnoreCase("DELETE"))
        {
            String jsonAdmin =
                    new BufferedReader(new InputStreamReader(exchange.getRequestBody()))
                            .lines()
                            .collect(Collectors.joining());

            JSONObject jsonObject = new JSONObject(jsonAdmin);
            int id = jsonObject.getInt("id");

            htmlBuilder.append("<html>")
                    .append("<body>")
                    .append("<h1>")
                    .append("Delete admin from Data Base: " + deleteMethod(id))
                    .append("</h1>")
                    .append("</body>")
                    .append("</html>");
        }

        String htmlStr = htmlBuilder.toString();
        exchange.sendResponseHeaders(200, htmlStr.length());

        outputStream.write(htmlStr.getBytes(StandardCharsets.UTF_8));
        outputStream.flush();
        outputStream.close();
    }

    private String getMethod()
    {
        return this.dao.readAllAdmins().toString();
    }

    private String postMethod(Admins admin)
    {
        try
        {
            this.dao.adAdmin(admin);
            return "SUCCESS";
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return "false";
        }
    }
    private String updateMethod(Admins admin, int id) {
        try {
            this.dao.updateAdmin(admin, id);
            return "SUCCESS";
        } catch (Exception e) {
            e.printStackTrace();
            return "false";
        }
    }
    private String deleteMethod(int id) {
        try {
            this.dao.deleteAdmin(id);
            return "SUCCESS";
        } catch (Exception e) {
            e.printStackTrace();
            return "false";
        }
    }
}
