package ems.handlers;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import ems.dao.TeachersDAO;
import ems.users.Teachers;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

public class TeachersHandler implements HttpHandler {
    private final TeachersDAO dao;

    public TeachersHandler(TeachersDAO dao)
    {
        this.dao = dao;
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException
    {
        OutputStream outputStream = exchange.getResponseBody();

        StringBuilder htmlBuilder = new StringBuilder();

        if (exchange.getRequestMethod().equalsIgnoreCase("GET"))
        {
            htmlBuilder.append("<html>")
                    .append("<body>")
                    .append("<h1>")
                    .append("All teachers from Data Base: " + getMethod())
                    .append("</h1>")
                    .append("</body>")
                    .append("</html>");
        }
        else if (exchange.getRequestMethod().equalsIgnoreCase("POST"))
        {
            String jsonTeacher =
                    new BufferedReader(new InputStreamReader(exchange.getRequestBody()))
                            .lines()
                            .collect(Collectors.joining());

            JSONObject jsonObject = new JSONObject(jsonTeacher);

            String name = jsonObject.toMap().get("name").toString();
            String surname = jsonObject.toMap().get("surname").toString();
            String email = jsonObject.toMap().get("email").toString();
            String password = jsonObject.toMap().get("password").toString();
            String subject = jsonObject.toMap().get("subject").toString();
            Teachers teacher = new Teachers(name, surname, email, password, subject);

            htmlBuilder.append("<html>")
                    .append("<body>")
                    .append("<h1>")
                    .append("Create new teacher in DB: " + postMethod(teacher))
                    .append("</h1>")
                    .append("</body>")
                    .append("</html>");
        }
        else if (exchange.getRequestMethod().equalsIgnoreCase("PUT"))
        {
            String jsonTeacher =
                    new BufferedReader(new InputStreamReader(exchange.getRequestBody()))
                            .lines()
                            .collect(Collectors.joining());

            JSONObject jsonObject = new JSONObject(jsonTeacher);

            String name = jsonObject.toMap().get("name").toString();
            String surname = jsonObject.toMap().get("surname").toString();
            String email = jsonObject.toMap().get("email").toString();
            String password = jsonObject.toMap().get("password").toString();
            String subject = jsonObject.toMap().get("subject").toString();
            Teachers teacher = new Teachers(name, surname, email, password, subject);
            int id = jsonObject.getInt("id");

            htmlBuilder.append("<html>")
                    .append("<body>")
                    .append("<h1>")
                    .append("Update teacher by ID in DB: " + updateMethod(teacher, id))
                    .append("</h1>")
                    .append("</body>")
                    .append("</html>");
        }
        else if (exchange.getRequestMethod().equalsIgnoreCase("DELETE"))
        {
            String jsonTeacher =
                    new BufferedReader(new InputStreamReader(exchange.getRequestBody()))
                            .lines()
                            .collect(Collectors.joining());

            JSONObject jsonObject = new JSONObject(jsonTeacher);
            int id = jsonObject.getInt("id");

            htmlBuilder.append("<html>")
                    .append("<body>")
                    .append("<h1>")
                    .append("Delete teacher from Data Base: " + deleteMethod(id))
                    .append("</h1>")
                    .append("</body>")
                    .append("</html>");
        }

        String htmlStr = htmlBuilder.toString();
        exchange.sendResponseHeaders(200, htmlStr.length());

        outputStream.write(htmlStr.getBytes(StandardCharsets.UTF_8));
        outputStream.flush();
        outputStream.close();
    }

    private String getMethod()
    {
        return this.dao.readAllTeachers().toString();
    }

    private String postMethod(Teachers teacher)
    {
        try
        {
            this.dao.adTeacher(teacher);
            return "SUCCESS";
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return "false";
        }
    }
    private String updateMethod(Teachers teacher, int id) {
        try {
            this.dao.updateTeacher(teacher, id);
            return "SUCCESS";
        } catch (Exception e) {
            e.printStackTrace();
            return "false";
        }
    }
    private String deleteMethod(int id) {
        try {
            this.dao.deleteTeacher(id);
            return "SUCCESS";
        } catch (Exception e) {
            e.printStackTrace();
            return "false";
        }
    }
}
