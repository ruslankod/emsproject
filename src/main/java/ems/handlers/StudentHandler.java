package ems.handlers;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import ems.dao.StudentsDAO;
import ems.users.Students;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

public class StudentHandler implements HttpHandler {
    private final StudentsDAO dao;

    public StudentHandler(StudentsDAO dao)
    {
        this.dao = dao;
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException
    {
        OutputStream outputStream = exchange.getResponseBody();

        StringBuilder htmlBuilder = new StringBuilder();

        if (exchange.getRequestMethod().equalsIgnoreCase("GET"))
        {
            htmlBuilder.append("<html>")
                    .append("<body>")
                    .append("<h1>")
                    .append("All students from Data Base: " + getMethod())
                    .append("</h1>")
                    .append("</body>")
                    .append("</html>");
        }
        else if (exchange.getRequestMethod().equalsIgnoreCase("POST"))
        {
            String jsonStudent =
                    new BufferedReader(new InputStreamReader(exchange.getRequestBody()))
                            .lines()
                            .collect(Collectors.joining());

            JSONObject jsonObject = new JSONObject(jsonStudent);

            String name = jsonObject.toMap().get("name").toString();
            String surname = jsonObject.toMap().get("surname").toString();
            String email = jsonObject.toMap().get("email").toString();
            String password = jsonObject.toMap().get("password").toString();
            String subject = jsonObject.toMap().get("subject").toString();
            Students student = new Students(name, surname, email, password, subject);

            htmlBuilder.append("<html>")
                    .append("<body>")
                    .append("<h1>")
                    .append("Create new student in DB: " + postMethod(student))
                    .append("</h1>")
                    .append("</body>")
                    .append("</html>");
        }
        else if (exchange.getRequestMethod().equalsIgnoreCase("PUT"))
        {
            String jsonStudent =
                    new BufferedReader(new InputStreamReader(exchange.getRequestBody()))
                            .lines()
                            .collect(Collectors.joining());

            JSONObject jsonObject = new JSONObject(jsonStudent);

            String name = jsonObject.toMap().get("name").toString();
            String surname = jsonObject.toMap().get("surname").toString();
            String email = jsonObject.toMap().get("email").toString();
            String password = jsonObject.toMap().get("password").toString();
            String subject = jsonObject.toMap().get("subject").toString();
            Students student = new Students(name, surname, email, password, subject);
            int id = jsonObject.getInt("id");

            htmlBuilder.append("<html>")
                    .append("<body>")
                    .append("<h1>")
                    .append("Update student by ID in DB: " + updateMethod(student, id))
                    .append("</h1>")
                    .append("</body>")
                    .append("</html>");
        }
        else if (exchange.getRequestMethod().equalsIgnoreCase("DELETE"))
        {
            String jsonStudent =
                    new BufferedReader(new InputStreamReader(exchange.getRequestBody()))
                            .lines()
                            .collect(Collectors.joining());

            JSONObject jsonObject = new JSONObject(jsonStudent);
            int id = jsonObject.getInt("id");

            htmlBuilder.append("<html>")
                    .append("<body>")
                    .append("<h1>")
                    .append("Delete student from Data Base: " + deleteMethod(id))
                    .append("</h1>")
                    .append("</body>")
                    .append("</html>");
        }

        String htmlStr = htmlBuilder.toString();
        exchange.sendResponseHeaders(200, htmlStr.length());

        outputStream.write(htmlStr.getBytes(StandardCharsets.UTF_8));
        outputStream.flush();
        outputStream.close();
    }

    private String getMethod()
    {
        return this.dao.readAllStudents().toString();
    }

    private String postMethod(Students student)
    {
        try
        {
            this.dao.adStudent(student);
            return "SUCCESS";
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return "false";
        }
    }
    private String updateMethod(Students student, int id) {
        try {
            this.dao.updateStudent(student, id);
            return "SUCCESS";
        } catch (Exception e) {
            e.printStackTrace();
            return "false";
        }
    }
    private String deleteMethod(int id) {
        try {
            this.dao.deleteStudent(id);
            return "SUCCESS";
        } catch (Exception e) {
            e.printStackTrace();
            return "false";
        }
    }
}